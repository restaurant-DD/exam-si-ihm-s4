<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index(){
		$this->load->database();
		$this->check_login();
	}	
	
	public function check_login() {
		$this->load->model('Users');
		$username = $this->input->post('username');
		$pwd = $this->input->post('pwd');
		$query = $this->db->get_where("Users", array("userName"=>$username, "pwd"=>SHA1($pwd)));
		$data['Users'] = $query->result();
		if($data['Users'] != null) {
			$query = $this->db->query("SELECT * FROM tables");
			$data['listTab'] = $query->result();
			$this->load->view('home',$data);
		} else {
			$error['err'] = "password or username invalid";
			$this->load->view('login', $error);
		}
	}
}
?>