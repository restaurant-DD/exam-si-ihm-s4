<?php 
    class Liste_Plats_Actuel_CTRL extends CI_Controller {
        function __construct() {
            parent::__construct();
            $this->load->database();
        }

        function index() {
            $platsDuJour = array();
            $query = $this->db->query("SELECT * FROM V_MENU_ACTUEL");
            $menusDuJour = $query->result();
            
            $cat = $this->input->get('cat');

            for($i=0; $i<count($menusDuJour); $i++){
                $sql = "SELECT * FROM V_PlatsDuJour_ACTUEL WHERE idCategoriesPlats='".$menusDuJour[$i]->idCategoriesPlats."'";
                $query = $this->db->query($sql);
                $platsDuJour[] = $query->result();
            }

            
            $query = $this->db->query("SELECT * FROM CategoriesPlats");
            $data['AllCateg'] = $query->result();

            $query = $this->db->query("SELECT * FROM Plats WHERE idPlats NOT IN (SELECT idPlats FROM V_PlatsDuJour_ACTUEL)");

            $data['menusDuJour'] = $menusDuJour;
            $data['platsDuJour'] = $platsDuJour;
            $data['platEnlever'] = $cat==null ? false : true;
            $data['cat'] = $cat;
            $data['autrePlats'] =  $query->result();
            $this->load->view('Liste_Plats_Actuel', $data);
        }

        function enlever(){
            $idPlatAEnlever = $this->input->get('idPlats');
            $cat = $this->input->get('cat');
            $this->db->delete("PlatsDuJour", "idPlatsDuJour = '".$idPlatAEnlever."'");
            Header('Location: '.base_url().'index.php/Liste_Plats_Actuel?cat='.$cat);
        }

        function ajouter(){
            
        }
    }
?>