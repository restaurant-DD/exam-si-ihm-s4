<?php 
   class PlatsDuJour_model extends CI_Model {
	
      function __construct() {
         parent::__construct(); 
      } 
   
      public function insert($data) { 
         if ($this->db->insert("PlatsDuJour", $data)) {
            return true; 
         }
      } 
   
      public function delete($roll_no) { 
         if ($this->db->delete("PlatsDuJour", "idPlatsDuJour = '".$roll_no."'")) {
            return true; 
         } 
      } 
   
      public function update($data,$old_roll_no) {
         $this->db->set($data); 
         $this->db->where("idPlatsDuJour", $old_roll_no); 
         $this->db->update("PlatsDuJour", $data);
      } 
   } 
?>