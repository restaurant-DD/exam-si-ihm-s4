CREATE DATABASE RESTO;
USE RESTO;

CREATE TABLE  Profils (
    idProfils VARCHAR(20) PRIMARY KEY,
    profilName VARCHAR(50)
);

CREATE TABLE  Droits (
    idDroits VARCHAR(20) PRIMARY KEY,
    idProfils VARCHAR(20),
    nomTable VARCHAR(50),
    droits VARCHAR(20),
    FOREIGN KEY (idProfils) REFERENCES Profils(idProfils)
);

CREATE TABLE  Users (
    idUsers VARCHAR(20) PRIMARY KEY,
    userName VARCHAR(100),
    sexe VARCHAR(1),
    idProfils VARCHAR(20),
    pwd VARCHAR(100),
    FOREIGN KEY (idProfils) REFERENCES Profils(idProfils)
);

CREATE TABLE  CategoriesPlats (
    idCategoriesPlats VARCHAR(20) PRIMARY KEY,
    categorie VARCHAR(100)
);

CREATE TABLE  Plats (
    idPlats VARCHAR(20) PRIMARY KEY,
    nomPlats VARCHAR(100),
    imagePlats VARCHAR(100),
    prix DECIMAL(10,2),
    idCategoriesPlats VARCHAR(20),
    etat INT,
    FOREIGN KEY (idCategoriesPlats) REFERENCES CategoriesPlats(idCategoriesPlats)
);

CREATE TABLE  Tables (
    idTables VARCHAR(20) PRIMARY KEY
);

CREATE TABLE  Commandes (
    idCommandes VARCHAR(20) PRIMARY KEY,
    idTables VARCHAR(20),
    idPlats VARCHAR(20),
    nombre INT,
    jour INT,
    mois INT,
    annee INT,
    etat INT,
    FOREIGN KEY (idTables) REFERENCES Tables(idTables),
    FOREIGN KEY (idPlats) REFERENCES Plats(idPlats)
);

CREATE TABLE  PlatsDuJour (
    idPlatsDuJour VARCHAR(20) PRIMARY KEY,
    jourSemaine INT,
    idPlats VARCHAR(20),
    etat INT,
    FOREIGN KEY (idPlats) REFERENCES Plats(idPlats)
);

insert into Profils (idProfils, profilName) values ('Profils 1', 'Serveur');
insert into Profils (idProfils, profilName) values ('Profils 2', 'Chef');
insert into Profils (idProfils, profilName) values ('Profils 3', 'Caissier');
insert into Profils (idProfils, profilName) values ('Profils 4', 'ADMIN');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 1', 'Profils 1', 'Commandes', 'C,R,U,D');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 2', 'Profils 1', 'PlatsDuJour', 'R');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 3', 'Profils 2', 'PlatsDuJour', 'C,R,U,D');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 4', 'Profils 2', 'Plats', 'C,R,U,D');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 5', 'Profils 2', 'Commandes', 'R');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 6', 'Profils 2', 'Users', 'C,R,U,D');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 7', 'Profils 4', 'ALL', 'ALL');


insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 8', 'Profils 3', 'Commandes', 'R,U');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 9', 'Profils 3', 'Plats', 'R');


insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 10', 'Profils 3', 'PlatsDuJour', 'R');

insert into Droits (idDroits, idProfils, nomTable, droits) values ('Droits 11', 'Profils 3', 'Tables', 'R');

--ADMIN
INSERT INTO Users VALUES ('Users 1', 'KOTO', 'H', 'Profils 4', SHA1('KOTO'));
--SEVEUR
INSERT INTO Users VALUES ('Users 2', 'SOA', 'F', 'Profils 1', SHA1('SOA'));
INSERT INTO Users VALUES ('Users 3', 'RICO', 'H', 'Profils 1', SHA1('RICO'));
INSERT INTO Users VALUES ('Users 4', 'VERO', 'F', 'Profils 1', SHA1('VERO'));
INSERT INTO Users VALUES ('Users 5', 'HANITRA', 'F', 'Profils 1', SHA1('HANITRA'));
INSERT INTO Users VALUES ('Users 6', 'KIALA', 'F', 'Profils 1', SHA1('KIALA'));
INSERT INTO Users VALUES ('Users 7', 'BAKO', 'F', 'Profils 1', SHA1('BAKO'));
--CHEFS
INSERT INTO Users VALUES ('Users 8', 'CHEF-MAURICE', 'F', 'Profils 2', SHA1('CHEF-MAURICE'));
INSERT INTO Users VALUES ('Users 9', 'CHEF-KIALA', 'F', 'Profils 2', SHA1('CHEF-KIALA'));
--CAISSIER
INSERT INTO Users VALUES ('Users 10', 'CAISSIER-GEN', 'H', 'Profils 3', SHA1('CAISSIER-GEN'));


INSERT INTO CategoriesPlats VALUES('Cat 1', 'JUS');
INSERT INTO CategoriesPlats VALUES('Cat 2', 'SOUPE');
INSERT INTO CategoriesPlats VALUES('Cat 3', 'RIZ');
INSERT INTO CategoriesPlats VALUES('Cat 4', 'TSAKY');


INSERT INTO Plats VALUES('Plats 1', 'COCA-COLA', '1.jpg', 1000, 'Cat 1', 1);
INSERT INTO Plats VALUES('Plats 2', 'BONBON-ANGLAIS', '2.jpg', 1000, 'Cat 1', 1);
INSERT INTO Plats VALUES('Plats 3', 'ORANGE', '3.jpg', 1000, 'Cat 1', 1);

INSERT INTO Plats VALUES('Plats 4', 'VAN-TAN-MINE', '4.jpg', 4000, 'Cat 2', 1);
INSERT INTO Plats VALUES('Plats 5', 'VAN-TAN-TSATSIU', '5.jpg', 7000, 'Cat 2', 1);
INSERT INTO Plats VALUES('Plats 6', 'VAN-TAN-SPECIALE', '6.jpg', 10000, 'Cat 2', 1);

INSERT INTO Plats VALUES('Plats 7', 'RIZ CANTONAIS', '7.jpg', 6000, 'Cat 3', 1);
INSERT INTO Plats VALUES('Plats 8', 'BOL RENVERSE', '8.jpg', 5000, 'Cat 3', 1);
INSERT INTO Plats VALUES('Plats 9', 'VARINAMPANGO', '9.jpg', 1800, 'Cat 3', 1);

INSERT INTO Plats VALUES('Plats 10', 'POULETS FRITES', '10.jpg', 1000, 'Cat 4', 1);
INSERT INTO Plats VALUES('Plats 11', 'MASKITA', '11.jpg', 200, 'Cat 4', 1);
INSERT INTO Plats VALUES('Plats 12', 'PAKOPAKO', '12.jpg', 300, 'Cat 4', 1);



INSERT INTO Tables VALUES('TABLE N° 1');
INSERT INTO Tables VALUES('TABLE N° 2');
INSERT INTO Tables VALUES('TABLE N° 3');
INSERT INTO Tables VALUES('TABLE N° 4');
INSERT INTO Tables VALUES('TABLE N° 5');


INSERT INTO Commandes VALUES('Commandes 1', 'TABLE N° 1', 'Plats 1', 5, 11, 7, 2019, 1);
INSERT INTO Commandes VALUES('Commandes 2', 'TABLE N° 1', 'Plats 4', 2, 11, 7, 2019, 1);
INSERT INTO Commandes VALUES('Commandes 3', 'TABLE N° 1', 'Plats 6', 1, 11, 7, 2019, 1);

INSERT INTO Commandes VALUES('Commandes 4', 'TABLE N° 2', 'Plats 11', 20, 11, 7, 2019, 1);
INSERT INTO Commandes VALUES('Commandes 5', 'TABLE N° 2', 'Plats 10', 30, 11, 7, 2019, 1);


INSERT INTO Commandes VALUES('Commandes 6', 'TABLE N° 5', 'Plats 1', 3, 11, 7, 2019, 2);
INSERT INTO Commandes VALUES('Commandes 7', 'TABLE N° 5', 'Plats 4', 2, 11, 7, 2019, 2);
INSERT INTO Commandes VALUES('Commandes 8', 'TABLE N° 5', 'Plats 6', 3, 11, 7, 2019, 2);

INSERT INTO Commandes VALUES('Commandes 9', 'TABLE N° 4', 'Plats 11', 10, 11, 7, 2019, 2);
INSERT INTO Commandes VALUES('Commandes 10', 'TABLE N° 4', 'Plats 10', 15, 11, 7, 2019, 1);


----------------------------------------------------------------------------------------
INSERT INTO Commandes VALUES('Commandes 11', 'TABLE N° 1', 'Plats 1', 5, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 12', 'TABLE N° 1', 'Plats 4', 2, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 13', 'TABLE N° 1', 'Plats 6', 1, 11, 7, 2018, 2);

INSERT INTO Commandes VALUES('Commandes 14', 'TABLE N° 2', 'Plats 11', 20, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 15', 'TABLE N° 2', 'Plats 10', 30, 11, 7, 2018, 2);



INSERT INTO Commandes VALUES('Commandes 16', 'TABLE N° 5', 'Plats 1', 3, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 17', 'TABLE N° 5', 'Plats 4', 2, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 18', 'TABLE N° 5', 'Plats 6', 3, 11, 7, 2018, 2);

INSERT INTO Commandes VALUES('Commandes 19', 'TABLE N° 4', 'Plats 11', 10, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 20', 'TABLE N° 4', 'Plats 10', 150, 11, 7, 2018, 2);

------------------------------------------------------------------------------------------
INSERT INTO Commandes VALUES('Commandes 21', 'TABLE N° 1', 'Plats 1', 1, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 22', 'TABLE N° 1', 'Plats 4', 2, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 23', 'TABLE N° 1', 'Plats 6', 1, 11, 7, 2017, 2);

INSERT INTO Commandes VALUES('Commandes 24', 'TABLE N° 2', 'Plats 11', 2, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 25', 'TABLE N° 2', 'Plats 10', 3, 11, 7, 2017, 2);



INSERT INTO Commandes VALUES('Commandes 26', 'TABLE N° 5', 'Plats 1', 3, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 27', 'TABLE N° 5', 'Plats 4', 2, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 28', 'TABLE N° 5', 'Plats 6', 3, 11, 7, 2017, 2);

INSERT INTO Commandes VALUES('Commandes 29', 'TABLE N° 4', 'Plats 11', 10, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 30', 'TABLE N° 4', 'Plats 10', 1, 11, 7, 2017, 2);
-------------------------------------------------
INSERT INTO Commandes VALUES('Commandes 31', 'TABLE N° 1', 'Plats 1', 4, 11, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 32', 'TABLE N° 1', 'Plats 4', 4, 11, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 33', 'TABLE N° 1', 'Plats 6', 4, 11, 7, 2016, 2);

INSERT INTO Commandes VALUES('Commandes 34', 'TABLE N° 2', 'Plats 11', 4, 11, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 35', 'TABLE N° 2', 'Plats 10', 4, 11, 7, 2016, 2);



INSERT INTO Commandes VALUES('Commandes 36', 'TABLE N° 5', 'Plats 1', 4, 11, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 37', 'TABLE N° 5', 'Plats 4', 4, 11, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 38', 'TABLE N° 5', 'Plats 6', 4, 11, 7, 2016, 2);

INSERT INTO Commandes VALUES('Commandes 39', 'TABLE N° 4', 'Plats 11', 20, 11, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 40', 'TABLE N° 4', 'Plats 10', 15, 11, 7, 2016, 2);
-------------------------------------------------
INSERT INTO Commandes VALUES('Commandes 51', 'TABLE N° 1', 'Plats 1', 400, 11, 7, 2015, 2);
INSERT INTO Commandes VALUES('Commandes 52', 'TABLE N° 1', 'Plats 4', 400, 11, 7, 2015, 2);
INSERT INTO Commandes VALUES('Commandes 53', 'TABLE N° 1', 'Plats 6', 400, 11, 7, 2015, 2);

INSERT INTO Commandes VALUES('Commandes 54', 'TABLE N° 2', 'Plats 11', 400, 11, 7, 2015, 2);
INSERT INTO Commandes VALUES('Commandes 55', 'TABLE N° 2', 'Plats 10', 400, 11, 7, 2015, 2);



INSERT INTO Commandes VALUES('Commandes 56', 'TABLE N° 5', 'Plats 1', 400, 11, 7, 2015, 2);
INSERT INTO Commandes VALUES('Commandes 57', 'TABLE N° 5', 'Plats 4', 400, 11, 7, 2015, 2);
INSERT INTO Commandes VALUES('Commandes 58', 'TABLE N° 5', 'Plats 6', 400, 11, 7, 2015, 2);

INSERT INTO Commandes VALUES('Commandes 59', 'TABLE N° 4', 'Plats 11', 400, 11, 7, 2015, 2);
INSERT INTO Commandes VALUES('Commandes 60', 'TABLE N° 4', 'Plats 10', 400, 11, 7, 2015, 2);
-------------------------------------------------
INSERT INTO Commandes VALUES('Commandes 71', 'TABLE N° 1', 'Plats 1', 40, 10, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 72', 'TABLE N° 1', 'Plats 4', 40, 10, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 73', 'TABLE N° 1', 'Plats 6', 40, 10, 7, 2016, 2);

INSERT INTO Commandes VALUES('Commandes 74', 'TABLE N° 2', 'Plats 11', 40, 10, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 75', 'TABLE N° 2', 'Plats 10', 40, 10, 7, 2016, 2);



INSERT INTO Commandes VALUES('Commandes 76', 'TABLE N° 5', 'Plats 1', 40, 10, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 77', 'TABLE N° 5', 'Plats 4', 40, 10, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 78', 'TABLE N° 5', 'Plats 6', 40, 10, 7, 2016, 2);

INSERT INTO Commandes VALUES('Commandes 79', 'TABLE N° 4', 'Plats 11', 40, 10, 7, 2016, 2);
INSERT INTO Commandes VALUES('Commandes 80', 'TABLE N° 4', 'Plats 10', 40, 10, 7, 2016, 2);
-----------------------------------------------------
INSERT INTO Commandes VALUES('Commandes 91', 'TABLE N° 1', 'Plats 1', 400, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 92', 'TABLE N° 1', 'Plats 4', 400, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 93', 'TABLE N° 1', 'Plats 6', 400, 11, 7, 2017, 2);

INSERT INTO Commandes VALUES('Commandes 94', 'TABLE N° 2', 'Plats 11', 400, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 95', 'TABLE N° 2', 'Plats 10', 400, 11, 7, 2017, 2);



INSERT INTO Commandes VALUES('Commandes 96', 'TABLE N° 5', 'Plats 1', 400, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 97', 'TABLE N° 5', 'Plats 4', 400, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 98', 'TABLE N° 5', 'Plats 6', 400, 11, 7, 2017, 2);

INSERT INTO Commandes VALUES('Commandes 99', 'TABLE N° 4', 'Plats 11', 400, 11, 7, 2017, 2);
INSERT INTO Commandes VALUES('Commandes 100', 'TABLE N° 4', 'Plats 10', 400, 11, 7, 2017, 2);
-----------------------------------------------------
INSERT INTO Commandes VALUES('Commandes 101', 'TABLE N° 1', 'Plats 1', 400, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 102', 'TABLE N° 1', 'Plats 4', 400, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 103', 'TABLE N° 1', 'Plats 6', 400, 11, 7, 2018, 2);

INSERT INTO Commandes VALUES('Commandes 104', 'TABLE N° 2', 'Plats 11', 400, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 105', 'TABLE N° 2', 'Plats 10', 400, 11, 7, 2018, 2);



INSERT INTO Commandes VALUES('Commandes 106', 'TABLE N° 5', 'Plats 1', 400, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 107', 'TABLE N° 5', 'Plats 4', 400, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 108', 'TABLE N° 5', 'Plats 6', 400, 11, 7, 2018, 2);

INSERT INTO Commandes VALUES('Commandes 109', 'TABLE N° 4', 'Plats 11', 400, 11, 7, 2018, 2);
INSERT INTO Commandes VALUES('Commandes 110', 'TABLE N° 4', 'Plats 10', 400, 11, 7, 2018, 2);
-----------------------------------------------------
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 1', 7, 'Plats 1', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 2', 7, 'Plats 2', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 3', 7, 'Plats 3', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 4', 7, 'Plats 4', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 5', 7, 'Plats 5', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 6', 7, 'Plats 6', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 7', 7, 'Plats 7', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 8', 7, 'Plats 8', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 9', 7, 'Plats 9', 1);

INSERT INTO PlatsDuJour VALUES('PlatsDuJour 10', 1, 'Plats 10', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 11', 1, 'Plats 1', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 13', 1, 'Plats 3', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 15', 1, 'Plats 5', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 17', 1, 'Plats 7', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 19', 1, 'Plats 9', 1);

INSERT INTO PlatsDuJour VALUES('PlatsDuJour 20', 2, 'Plats 10', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 21', 2, 'Plats 11', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 23', 2, 'Plats 3', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 25', 2, 'Plats 5', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 27', 2, 'Plats 7', 1);

INSERT INTO PlatsDuJour VALUES('PlatsDuJour 31', 3, 'Plats 1', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 32', 3, 'Plats 2', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 33', 3, 'Plats 3', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 34', 3, 'Plats 4', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 35', 3, 'Plats 5', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 36', 3, 'Plats 6', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 37', 3, 'Plats 7', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 38', 3, 'Plats 8', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 39', 3, 'Plats 9', 1);

INSERT INTO PlatsDuJour VALUES('PlatsDuJour 41', 4, 'Plats 10', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 42', 4, 'Plats 11', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 43', 4, 'Plats 12', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 44', 4, 'Plats 4', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 46', 4, 'Plats 6', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 48', 4, 'Plats 8', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 49', 4, 'Plats 9', 1);

INSERT INTO PlatsDuJour VALUES('PlatsDuJour 61', 5, 'Plats 10', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 62', 5, 'Plats 11', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 63', 5, 'Plats 12', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 64', 5, 'Plats 4', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 66', 5, 'Plats 5', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 67', 5, 'Plats 6', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 68', 5, 'Plats 8', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 69', 5, 'Plats 7', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 60', 5, 'Plats 9', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 70', 5, 'Plats 1', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 71', 5, 'Plats 2', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 72', 5, 'Plats 3', 1);

INSERT INTO PlatsDuJour VALUES('PlatsDuJour 81', 6, 'Plats 10', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 82', 6, 'Plats 11', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 83', 6, 'Plats 12', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 84', 6, 'Plats 4', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 86', 6, 'Plats 5', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 87', 6, 'Plats 6', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 89', 6, 'Plats 7', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 90', 6, 'Plats 9', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 91', 6, 'Plats 1', 1);
INSERT INTO PlatsDuJour VALUES('PlatsDuJour 92', 6, 'Plats 3', 1);

CREATE VIEW V_PlatsDuJour AS(
    SELECT 
        idPlatsDuJour, jourSemaine, PlatsDuJour.etat  as etatPlatsDuJour, idPlats, nomPlats , imagePlats , prix, Plats.etat as etatPlats, idCategoriesPlats, categorie
    FROM
        PlatsDuJour NATURAL JOIN Plats NATURAL JOIN CategoriesPlats
);

CREATE VIEW V_PlatsDuJour_ACTUEL AS (
    SELECT * FROM V_PlatsDuJour WHERE jourSemaine IN (SELECT DAYOFWEEK(CURRENT_DATE())) AND etatPlatsDuJour=1 ORDER BY categorie
);

CREATE VIEW V_MENU_ACTUEL AS(
    SELECT DISTINCT(categorie),idCategoriesPlats FROM  V_PlatsDuJour_ACTUEL
);

CREATE VIEW V_TB_BORD AS (
    select SUM(nombre * prix) as total_vente, annee  from commandes,Plats where Plats.idPlats = commandes.idPlats AND commandes.etat=2 GROUP BY annee
);

CREATE VIEW V_COMANDE_PAR_TABLE AS (
    SELECT idTables , Commandes.idPlats  as id_Plats, nombre , jour , mois , annee , Commandes.etat as commandesEtat, nomPlats , imagePlats , prix , idCategoriesPlats , Plats.etat as etatPlats FROM Commandes,Plats  WHERE Commandes.idPlats=Plats.idPlats AND Commandes.etat=1 ORDER BY Commandes.idTables
);