<?php
class Tb_Bord_Ctrl extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function index(){
        $query = $this->db->query("SELECT * FROM V_TB_BORD");
        $tbord = $query->result();
        $data['tbord'] = $tbord;
        $this->load->view('Tableau_B', $data);
    }
}
?>