<?php 
if($platEnlever){
    echo "<script type=\"text/javascript\">window.location.assign(\"#".$cat."\");</script>";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Les Plats du jours</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url();?>assets/Template/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/Template/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="<?php echo base_url();?>assets/MY-CSS/style.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url();?>assets/Template/css/grayscale.min.css" rel="stylesheet">

</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav">
        <?php for($i=0; $i<count($menusDuJour); $i++){?>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#<?php echo $menusDuJour[$i]->categorie;?>"><?php echo $menusDuJour[$i]->categorie;?></a>
          </li>
        <?php }?>

            <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#AjouterPlats">Ajouter Plats</a>
          </li>
          
        </ul>
      </div>
    </div>
  </nav>
<!-- Header -->
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="text-center">
        <h1>LES PLATS DU JOUR</h1>
      </div>
    </div>
  </header>
  
<?php for($i=0; $i<count($menusDuJour); $i++){?>
    <section id="<?php echo $menusDuJour[$i]->categorie;?>" class="projects-section bg-light" class="SectionListPlatActuel">
        <div class="container">
            <h2>CATEGORIE: <?php echo $menusDuJour[$i]->categorie;?></h2>
            <table class="table table-bordered">
                <tr>
                    <th>Nom du plat</th>
                    <th>Prix</th>
                </tr>
                <?php for($t=0; $t<count($platsDuJour[$i]); $t++){?>
                    <tr>
                        <td><?php echo $platsDuJour[$i][$t]->nomPlats; ?></td>
                        <td class="chiffres"><?php echo $platsDuJour[$i][$t]->prix; ?> Ar</td>
                        <td><a href="<?php echo base_url();?>index.php/Liste_Plats_Actuel/Enlever_Plat?idPlats=<?php echo $platsDuJour[$i][$t]->idPlatsDuJour; ?>&&cat=<?php echo $platsDuJour[$i][$t]->categorie; ?>" class="btn btn-lg btn-warning btnEnleverPlat">Enlever</a></td>
                    </tr>
                <?php }?>
            </table>
        </div>
    </section>
<?php }?>

    <section id="AjouterPlats" class="projects-section bg-light SectionListPlatActuel">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Ajouter un Plat</h2>
                    <form action="Liste_Plats_Actuel/Ajouter" mehod="post">
                        <div class="form-group">
                            <label for="PlatNotInActuel">Autres Plats</label>
                               
                            <select name="idPlats" id="idPlats">
                                <?php for($i=0; $i<count($autrePlats); $i++){?>
                                    <option value="<?php echo $autrePlats[$i]->idPlats;?>" class="form-control"><?php echo $autrePlats[$i]->nomPlats;?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Ajouter Le Plat" class="btn btn-success">
                        </div>
                    </form>
                </div>

                <div class="col-md-6">
                    <h2>Insérer Un Nouveau Plat</h2>
                    <form action="">
                        <div class="form-group">
                            <label for="PlatNotInActuel">Nom du Plat</label>
                            <input type="text" name="nomPlat" id="nomPlat" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="ListeCat">Catégorie</label>
                            <select name="Categ" id="Categ">
                                <?php for($i=0; $i<count($AllCateg); $i++){?>
                                    <option value="<?php echo $AllCateg[$i]->idCategoriesPlats;?>" id="Categ" class="form-control"><?php echo $AllCateg[$i]->categorie;?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="imagePlat">Image</label>
                            <input type="text" name="imagePlat" id="imagePlat" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Céer Le Plat" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url();?>assets/Template/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/Template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url();?>assets/Template/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url();?>assets/Template/js/grayscale.min.js"></script>

</body>

</html>