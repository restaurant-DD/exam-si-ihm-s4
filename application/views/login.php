<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>LOGIN</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/MY-CSS/style.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
</head>
<body>
<header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(<?php echo base_url();?>assets/images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay">
			<img src="<?php echo base_url();?>assets/images/bistrot.jpg" width="470px">
			<img src="<?php echo base_url();?>assets/images/serveur.jpg" width="500px">
			<img src="<?php echo base_url();?>assets/images/bis.jpg" width="370px">
		</div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Savory <a href="#" target="_blank">Resto</a></span>
							<h1 class="cursive-font">All in good!</h1>	
						</div>
						<div class="col-md-4 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
							<div class="form-wrap">
								<div class="tab">	
									
									<div class="tab-content">
										<div class="tab-content-inner active" data-content="signup">
											<h3 class="cursive-font">Login Users</h3>
											<form action="<?php echo base_url();?>index.php/home"  method="POST">
												<div class="row form-group">
													<div class="col-md-12">
														<label for="username">Username</label>
														<input type="text" name="username" class="form-control bg-dark">
													</div>
												</div>
												<div class="row form-group">
													<div class="col-md-12">
														<label for="pwd">Password</label>
														<input type="text" name="pwd" class="form-control bg-dark">
													</div>
												</div>

												<div class="row form-group">
													<div class="col-md-12">
														<input type="submit" class="btn btn-primary btn-block" value="Sign in ">
													</div>
													<?php 
														if(!isset($err)){
															echo $err = "";
														} else {
															echo $err;
														}
														
													?>
												</div>
											</form>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
							
					
				</div>
			</div>
		</div>
	</header>
	<script src="<?php echo base_url();?>assets/dist/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/dist/js/popper.min.js"></script>
	<script src="<?php echo base_url();?>assets/dist/js/bootstrap.min.js"></script>
</body>
</html>